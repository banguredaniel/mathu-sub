#include <RH_ASK.h>
#include <SPI.h>

RH_ASK rf_driver;

struct Signal {
  int throttle;
  short rudder;
  int pump;
};

typedef struct Signal Data;
Data data;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  data.throttle = 0;
  data.rudder = 0;
  data.pump = 0;
  rf_driver.init();
}

void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);
 
  if (rf_driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);
    Serial.println(data.throttle);
    Serial.println(data.rudder); 
    Serial.println(data.pump);  
  }
}
