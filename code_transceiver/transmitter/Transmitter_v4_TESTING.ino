#include <RH_ASK.h>
#include <SPI.h>

const uint8_t MX = A0;
const uint8_t MY = A1;
const uint8_t PY = A2;

RH_ASK driver;

struct Signal
{
  short throttleF;
  short throttleB;
  int rudder;
  short pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

void mapping(int motorX, int motorY, int pumpY){
  if(motorY > 560){
    data.throttleF = map(motorY, 560, 1023, 0, 255);
    data.throttleB = 0;
  }
  else if(motorY < 464){
    data.throttleF = 0;
    data.throttleB = map(motorY, 0, 464, 255, 0);
  }
  else {
    data.throttleF = 0;
    data.throttleB = 0;
  }
  data.rudder = map(motorX, 0, 1023, 775, 2225);

  if(pumpY > 560){
    data.valve = true;
    data.pump = map(pumpY, 560, 1023, 0, 255);
  }
  else if(pumpY < 464){
    data.valve = false;
    data.pump = map(pumpY, 0, 560, 255, 0);
  }
  else{
    data.valve = false;
    data.pump = 0;
  }
  /*Serial.print(data.throttleF);
  Serial.print(", ");
  Serial.println(data.throttleB);*/
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  mapping(analogRead(MX), analogRead(MY), analogRead(PY));
  driver.send((uint8_t *)&data, sizeof(data));
  driver.waitPacketSent();
/*
  Serial.print(data.throttle);
  Serial.print(", ");
  Serial.print(data.rudder);
  Serial.print(", ");
  Serial.println(data.pump);
  */
}
